﻿using UnityEngine;
using System.Collections;

public class OnCollision:MonoBehaviour {

	public float hoverforce=12f;

	// Use this for initialization
	void OnCollisionEnter(Collision collision){
		Debug.Log("Enter Collision");
	}
	void OnCollisionStay(Collision collision){
		Debug.Log("Stay Collision");
	}
	void OnCollisionExit(Collision collision){
		Debug.Log("Collision Exit");
	}
	void OnTriggerEnter(Collider collider){
		Debug.Log("Enter Collider");
	}
	void OnTriggerStay(Collider collider){
		Debug.Log("Stay Collider");
		collider.rigidbody.AddForce (Vector3.up * hoverforce, ForceMode.Acceleration);
	}
	void OnTriggerExit(Collider collider){
		Debug.Log("Collider Exit");
	}
}
